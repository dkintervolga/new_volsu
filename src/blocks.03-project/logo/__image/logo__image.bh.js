module.exports = function(bh) {
    bh.match('logo__image', function(ctx, json) {
        ctx.tag('img');
        ctx.attrs({
            src: json.src,
            title: 'Волгоградский государственный университет',
            alt: 'ВолГУ',
        });
    });
};

