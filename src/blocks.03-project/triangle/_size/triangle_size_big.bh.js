module.exports = function(bh) {
    bh.match('triangle_size_big', function(ctx, json) {
        var id = ctx.generateId();
        var size = [Math.floor(Math.random() * 300), Math.floor(Math.random() * 300)];
        var isRandom = true;
        ctx.html(`
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="259px" height="247px">
            <defs>
                <pattern id="${id}" patternUnits="userSpaceOnUse" width="260" height="260">
                    <image xlink:href="${json.src}" x="0" y="0" width="260" height="260"/>
                </pattern>
            </defs>
            <path fill="url(#${id})" d="M14.192,37.764 C26.110,35.862 235.301,2.482 243.523,1.170 C254.185,-0.532 262.118,9.187 255.880,21.037 C250.886,30.533 150.067,220.480 141.445,236.694 C135.780,247.343 122.273,249.335 116.602,240.658 C110.278,230.986 8.945,72.633 1.906,61.118 C-3.258,52.688 2.446,39.638 14.192,37.764 Z"/>
            </svg>
            `);
    });
};
