module.exports = function(bh) {
    bh.match('triangle_noimage', function(ctx, json) {
        ctx.html('<svg xmlns="http://www.w3.org/2000/svg" ' +
            'xmlns:xlink="http://www.w3.org/1999/xlink" width="41px" ' +
            'height="39px"><path fill-rule="evenodd"' +
            ' d="M2.423,5.855 C4.308,5.552 37.397,0.240 38.698,0.031 ' +
            'C40.384,-0.240 41.639,1.307 40.653,3.193 C39.863,4.704 ' +
            '23.915,34.932 22.551,37.512 C21.655,39.207 19.519,39.524 ' +
            '18.622,38.143 C17.621,36.604 1.593,11.404 0.479,9.571 ' +
            'C-0.337,8.230 0.565,6.153 2.423,5.855 Z"/></svg>');
    });
};

