module.exports = function(bh) {
    bh.match('link', function(ctx, json) {
        ctx.tag('a');
        ctx.attrs({href: '#'});
        
        if(json.slide) {
            ctx.attrs({'data-slide': json.slide});
        }
    });
};

