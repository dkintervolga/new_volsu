$(document).ready(function() {
    $('.link_toggle-tags').on('click', function(event) {
        event.preventDefault();
        $('.tags').toggleClass('tags_show');
        $(this).text(($('.tags_show').is(':visible')) ? $(this).data('hide') : $(this).data('show') );
    });
});
