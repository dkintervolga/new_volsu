module.exports = function(bh) {
    bh.match('hotline__phone', function(ctx, json) {
        ctx.tag('a');
        ctx.attrs({'href': 'tel:' + json.tel});
    });
};

