import Swiper from 'swiper/dist/js/swiper.js';

var slider = new Swiper('.slider_main .swiper-container', {
    slidesPerView: 1,
    slidesPerGroup: 1,
    spaceBetween: 40,
    loop: true,
    loopFillGroupWithBlank: true,
    autoplay: {
        delay: 3000,
        disableOnInteraction: false,
    },
    setWrapperSize: true,
    wrapperClass: 'slider__wrapper',
    slideClass: 'slider__slide',
    navigation: {
        nextEl: '.slider__arrow_arrow_right',
        prevEl: '.slider__arrow_arrow_left',
    },
    pagination: {
        el: '.slider__pagination',
        bulletClass: 'slider__bullet',
        bulletActiveClass: 'slider__bullet_selected',
        clickable: true,
    },
    on: {
        slideChange: function () {
            var view = 'link_slide-view',
                dotView = '.' + view,
                wrapper = $('.main-slider__images-wrapper'),
                index = this.realIndex,
                link = '';

            wrapper.find(dotView).toggleClass(view);
            wrapper.find('.img').filter(function() {
                link = $(this).parents('.link');

                if(link.data('slide') == index) {
                    link.toggleClass(view);
                }
            });
        },
    },
});

var slider_banners = new Swiper('.slider-banners', {
    slidesPerView: 6,
    slidesPerGroup: 1,
    spaceBetween: 18,
    width: 968,
    loop: true,
    loopFillGroupWithBlank: true,
    autoplay: {
        delay: 4000,
        disableOnInteraction: false,
    },
    wrapperClass: 'slider-banners__wrapper',
    slideClass: 'slider-banners__slide',
    navigation: {
        nextEl: '.icon-arr_right',
        prevEl: '.icon-arr_left',
    },
    breakpoints: {
        575: {
            slidesPerView: 1,
            width: 290
        },
        767: {
            slidesPerView: 2,
            width: 335
        },
        991: {
            slidesPerView: 3,
            width: 500
        },
        1199: {
            slidesPerView: 5,
            width: 760
        },
    }
});

var slider_blocks = new Swiper('.slider-blocks', {
    slidesPerView: 4,
    slidesPerGroup: 1,
    spaceBetween: 28,
    loop: true,
    loopFillGroupWithBlank: true,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    wrapperClass: 'slider-blocks__wrapper',
    slideClass: 'slider-blocks__slide',
    pagination: {
        el: '.slider-blocks__pagination',
        bulletClass: 'slider__bullet',
        bulletActiveClass: 'slider__bullet_selected',
        clickable: true,
    },
    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 0
        },
        575: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        991: {
            slidesPerView: 2,
            spaceBetween: 10
        },
        1199: {
            slidesPerView: 3,
            spaceBetween: 70
        }
    }
});

var slider_socials = new Swiper('.slider-socials', {
    slidesPerView: 5,
    slidesPerGroup: 1,
    width: 970,
    loop: true,
    loopFillGroupWithBlank: true,
    autoplay: {
        delay: 3000,
        disableOnInteraction: false,
    },
    wrapperClass: 'slider-socials__wrapper',
    slideClass: 'slider-socials__slide',
    navigation: {
        nextEl: '.icon-arr_right',
        prevEl: '.icon-arr_left',
    },
    breakpoints: {
        575: {
            slidesPerView: 1,
            width: 290,
        },
        767: {
            slidesPerView: 2,
            width: 380,
        },
        991: {
            slidesPerView: 3,
            width: 560,
        },
        1199: {
            slidesPerView: 4,
            width: 800,
        },
    },
});

var slider_gallery = new Swiper('.slider_gallery', {
    observer: true,
    slidesPerView: 5,
    slidesPerGroup: 1,
    spaceBetween: 29,
    width: 945,
    loop: true,
    loopFillGroupWithBlank: true,
    wrapperClass: 'photogallery__box_grid',
    slideClass: 'photogallery__slide',
    navigation: {
        nextEl: '.icon-arr_right',
        prevEl: '.icon-arr_left',
    },
    breakpoints: {
        575: {
            slidesPerView: 1,
            width: 165,
        },
        767: {
            slidesPerView: 2,
            spaceBetween: 29,
            width: 360,
        },
        991: {
            slidesPerView: 3,
            spaceBetween: 29,
            width: 555,
        },
        1199: {
            slidesPerView: 4,
            spaceBetween: 29,
            width: 747,
        },
    },
    on: {
        init: function() {
            var allSlides = $(this.wrapperEl).data('slides');

            $(this.wrapperEl).wrap('<div class="photogallery__box-wrapper"></div>');

            if(this.currentBreakpoint === "575") {
                $('.photogallery__slide .link').unwrap();
                $(this.wrapperEl).find('.link').wrap('<div class="photogallery__slide"></div>');
                this.update();
            }

            var current = $('.photogallery__current').first();

            $('.photogallery__img-wrapper img').attr('src', current.data('img'));
            $('.photogallery__descr').text(current.data('descr'));

            $('.photogallery__cnt-num_cur').text('1');
            $('.photogallery__cnt-num_all').text( allSlides );
        }
    },
});