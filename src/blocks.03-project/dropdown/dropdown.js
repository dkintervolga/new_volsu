$(document).ready(function() {
    /*
    * Показывает выпадающее меню
    **/
    function showDropdown(elem) {
        var list = elem.find('.dropdown__list');
        var top = list.outerHeight(true);

        list.css({'top': -top + 'px'});
        elem.toggleClass('dropdown_open');
    }
    /*
    * Скрывает выпадающее меню
    **/
    function closeDropdown(elem) {
        elem.toggleClass('dropdown_open');
    }

    $('body')
        .on('click', function(elem) {
            var classDropdown = 'dropdown_feedback';
            var dotClassDropdown = '.dropdown_feedback';

            if($('.dropdown__list:hidden').length < 1) {
                closeDropdown($(dotClassDropdown));
            } else {
                if(elem.target.classList.contains(classDropdown)) {
                    showDropdown($(dotClassDropdown));
                }
            }
        });
});
