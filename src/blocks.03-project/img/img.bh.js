module.exports = function(bh) {
    bh.match('img', function(ctx, json) {
        ctx.tag('img');
        ctx.attrs({
            src: json.src,
            alt: '',
            title: '',
        });

        if(json.slide) {
            ctx.attrs({'data-slide': json.slide});
        }
    });
};

