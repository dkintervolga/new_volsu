$(document).ready(function() {
    $('body').on('click', '.card', function(elem) {
        if($(this).data('toggle')) {
            var type = $(this).data('type');
            var modal = $('.modal_media');

            modal.find('.modal__body').css({'display': 'none'});
            modal.find('.modal__body_' + type).css({'display': 'block'});

            modal.modal('show');
        } else {
            var mark = $(elem.target);

            if(mark.hasClass('mark')) {
                elem.preventDefault();

                var tags = $('.tags');
                var curTag = mark.data('tag');

                // Show block with tags
                if(!tags.hasClass('tags_show')) {
                    $('.link_toggle-tags').click();
                }

                // Animate scroll to tags
                $('html, body').animate({
                    scrollTop: $('.events-page__content').offset().top,
                }, 350);

                // Select tag
                $('.tags__item').each(function() {
                    if($(this).data('tag') === curTag) {
                        $(this).find('.link').click();
                        return false;
                    }
                });
            }
        }
    });
});
