(function($) {
    /**
     * Renders an image in gray scale using canvas
     * @param {Boolean} toggle - Optional, whether or not to add or remove the grayscale effect
     * @return {jQuery}
     *
     * @author https://gist.github.com/victorjonsson/2723583
     */
    $.fn.grayScale = function(toggle) {
        function _grayscale(src, w, h) {
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var imgObj = new Image();
            imgObj.src = src;
            canvas.width = w;
            canvas.height = h;
            ctx.drawImage(imgObj, 0, 0);
            var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
            for(var y = 0; y < imgPixels.height; y++) {
                for(var x = 0; x < imgPixels.width; x++) {
                    var i = (y * 4) * imgPixels.width + x * 4;
                    var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
                    imgPixels.data[i] = avg;
                    imgPixels.data[i + 1] = avg;
                    imgPixels.data[i + 2] = avg;
                }
            }
            ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
            return canvas.toDataURL();
        }

        return this.each(function() {
            var $img = $(this);
            if(this.nodeName == 'IMG') {
                if(toggle === false) {
                    var origSrc = $img.attr('data-src');
                    if(origSrc) {
                        this.src = $img.attr('data-src');
                        $img.removeAttr('data-src');
                    }
                } else if( !$img.attr('data-src') ) {
                    $img.attr('data-src', this.src);
                    this.src = _grayscale($(this).attr('src'), $(this).width(), $(this).height());
                }
            }
        });
    };
})(jQuery);

$(document).ready(function() {
    var imgSrc = '';

    $('.slider-banners__img')
        .delay(1000)
        .grayScale()
        .hover(function() {
            imgSrc = $(this).attr('src');
            $(this).attr('src', $(this).data('src'));
        }, function() {
            $(this).attr('src', imgSrc);
        });
});
