module.exports = function(bh) {
    bh.match('pagination__item_disabled', function(ctx, json) {
        ctx.attrs({tabindex: -1});
    });
};

