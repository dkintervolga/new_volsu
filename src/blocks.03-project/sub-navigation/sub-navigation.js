$(document).ready(function() {
    $('.sub-navigation').on('click', '.sub-navigation__item', function(event) {
        var selectedClass = 'sub-navigation__item_selected';

        if(!$(this).hasClass(selectedClass)) {
            $('.sub-navigation__item_selected').removeClass(selectedClass);
            $(this).addClass(selectedClass);
        } else {
            event.preventDefault();
        }
    });
});
