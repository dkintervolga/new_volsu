module.exports = function(bh) {
    bh.match('sub-navigation__link', function(ctx, json) {
        ctx.tag('a');
        ctx.attrs({href: '#'});
    });
};
