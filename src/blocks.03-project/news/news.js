$(document).ready(function() {
    var filterSelectedClass = 'filter__control_selected';
    var dotFilterSelectedClass = '.' + filterSelectedClass;
    var filterDefText = 'Все новости';
    var tooltipRightClass = 'news__descr-wrapper_right';
    var tooltipCenterClass = 'news__descr-wrapper_center';
    var itemHideClass = 'news__item-box_hide';
    var dotItemHideClass = '.' + itemHideClass;
    var showItemClass = 'news__item_show';
    var dotShowItemClass = '.' + showItemClass;
    var isTouch = false;

    $('[data-toggle="popover"]').popover();

    /**
     * Добавляет данные из 4-х первых элементов с новостями в "треугольники"
     */
    function addDataToTriangles() {
        var news = $('.news__content > .news__item-box').slice(0, 4);
        var triangles = $('.news__top-news > .news__item-box');
        var imgPath = '';
        var descr = '';
        var elem = '';

        triangles.filter(function(index) {
            elem = $(news[index]);
            imgPath = elem.find('.news__photo').attr('data-large');
            descr = elem.find('p.news__descr').text();

            $(this).find('pattern image').attr('xlink:href', imgPath);
            $(this).find('.news__descr').text( descr );
        });
    }

    /*
    * Скрывает превью новости
    **/
    function hideTooltip(elem) {
        var tooltip = elem.find('.news__descr-wrapper');

        elem.removeClass('news__item_show');

        if (tooltip.hasClass(tooltipRightClass)) {
            tooltip.removeClass(tooltipRightClass);
        }

        if (tooltip.hasClass(tooltipCenterClass)) {
            tooltip.removeClass(tooltipCenterClass);
        }
    }

    $('.news').on('click', '.filter__control', function() {
        var elem = $(this);
        var mark = $('.news .block-header .mark');
        var filterFrom = elem.data('filter');
        var filterParentElem = elem.parents('.filter');
        var filterText = '';
        var newsItems = $('.news__item');
        var filtersCnt = $('.filter__control').length;

        elem.toggleClass(filterSelectedClass);

        switch($(dotFilterSelectedClass).length) {
            case 0:
            case filtersCnt:
                filterText = filterDefText;
                break;
            case 1:
                filterText = $(dotFilterSelectedClass).text();
                break;
            default:
                filterText = 'Фильтр';
                break;
        }

        mark.text( filterText );
    });

    $('.news__item_little').on('mouseenter', function() {
        var elem = $(this);
        var tooltip = elem.find('.news__descr-wrapper');
        var tooltipTop = tooltip.outerHeight(true) + 8;
        var tooltipWidth = tooltip.outerWidth(true);
        var itemPosLeft = elem.position().left;
        var itemWidth = elem.outerWidth(true);
        var photoWidth = elem.find('.news__photo-wrapper').outerWidth();
        var photoPosLeft = (itemWidth - photoWidth) / 2;
        var parentWidth = elem.parents('.news__content').outerWidth();

        if((parentWidth - (itemPosLeft + photoPosLeft)) < tooltipWidth) {
            if((itemPosLeft + photoPosLeft + photoWidth) < tooltipWidth) {
                tooltip.addClass(tooltipCenterClass);
            } else {
                if(!tooltip.hasClass(tooltipRightClass)) {
                    tooltip.addClass(tooltipRightClass);
                }
            }
        }

        setTimeout(function() {
            if ((elem.is(':hover')) || (isTouch)) {
                tooltip.css({'top': -tooltipTop + 'px'});
                elem.addClass('news__item_show');
            }
        }, 100);
    }).on('mouseleave', function() {
        var elem = $(this);

        setTimeout(function() {
            if(!elem.is(':hover')) {
                hideTooltip(elem);
            }
        }, 200);
    });

    $('.news__photo-wrapper').on('touchstart', function(event) {
        var content = $('.news__content').find('.news__descr-wrapper');

        event.preventDefault();
        $(dotShowItemClass).removeClass(showItemClass);
        hideTooltip( content );
        isTouch = true;
        $(this).mouseenter();
    });

    $('body').on('touchstart', function(event) {
        if(!$(event.target).hasClass('news__photo-wrapper')) {
            $('.news__item_little').mouseleave();
        }
    });

    addDataToTriangles();
});
