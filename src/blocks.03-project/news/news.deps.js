({
    shouldDeps: [
        {
            elem: 'descr-wrapper',
            mod: 'right',
            val: true,
        },
        {
            elem: 'descr-wrapper',
            mod: 'center',
            val: true,
        },
        {
            elem: 'item-box',
            mod: 'hide',
            val: true,
        },
        {
            elem: 'item',
            mod: 'show',
            val: true,
        },
    ],
});
