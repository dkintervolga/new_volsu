({
    mustDeps: [
        {block: 'font-philosopher'},
        {
            block: 'links-replace',
            mod: 'top',
        },
        {
            block: 'dropdown',
            mod: 'open',
            val: true,
        },
        {
            block: 'container',
        },
        {
            block: 'link',
            mod: 'slide-view',
            val: true,
        },
    ],
});
