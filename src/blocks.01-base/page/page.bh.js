module.exports = function(bh) {
  bh.match('page', function(ctx, json) {
    var noscriptWarning = json.noscriptWarning
        || 'В вашем браузере отключен JavaScript. ' +
        'Многие элементы сайта могут работать некорректно.',
      oldBrowserWarning = json.oldBrowserWarning ||
        'Ваш браузер устарел и не обеспечивает полноценную и безопасную ' +
        'работу с сайтом. Пожалуйста <a rel="nofollow" ' +
        'onclick="window.open(this.href, \'_blank\');return false;" ' +
        'href="https://browsehappy.com/">обновите браузер</a>, чтобы ' +
        'улучшить взаимодействие с сайтом.';

    ctx
      .tag('body')
      .bem(false)
      .content([
        '<!--noindex-->',
        {elem: 'noscript', content: noscriptWarning},
        {elem: 'browsehappy', content: oldBrowserWarning},
        '<!--/noindex-->',
        ctx.content(),
        json.scripts,
      ], true);

    return [
      {html: '<!DOCTYPE html>', tag: false},
      {
        tag: 'html',
        attrs: {lang: json.lang || 'ru'},
        cls: 'ua-no-js',
        content: [
          {
            elem: 'head',
            content: [
              {tag: 'meta', attrs: {charset: 'utf-8'}},
              {
                tag: 'meta',
                attrs: {
                  name: 'viewport',
                  content: 'width=device-width, initial-scale=1, ' +
                  'shrink-to-fit=no',
                },
              },
              {
                tag: 'meta',
                attrs: {
                    name: 'cmsmagazine',
                    content: 'c305d0a70e715064e171fbd344a4b2dd'
                }
              },
              {
                  tag: 'link',
                  attrs: {
                      rel: 'shortcut icon',
                      href: 'favicons/favicon.ico',
                  },
              },
                {
                    tag: 'link',
                    attrs: {
                        rel: 'shortcut icon',
                        href: 'favicons/favicon-32x32.png',
                    },
                },
                {
                    tag: 'link',
                    attrs: {
                        rel: 'apple-touch-icon',
                        sizes: '57x57',
                        href: 'favicons/touch-icon-57x57-iphone.png',
                    },
                },
                {
                    tag: 'link',
                    attrs: {
                        rel: 'apple-touch-icon',
                        sizes: '76x76',
                        href: 'favicons/touch-icon-76x76-ipad.png',
                    },
                },
                {
                    tag: 'link',
                    attrs: {
                        rel: 'apple-touch-icon',
                        sizes: '120x120',
                        href: 'favicons/touch-icon-120x120-iphone-retina.png',
                    },
                },
                {
                    tag: 'link',
                    attrs: {
                        rel: 'apple-touch-icon',
                        sizes: '152x152',
                        href: 'favicons/touch-icon-152x152-ipad-retina.png',
                    },
                },
                {
                    tag: 'link',
                    attrs: {
                        rel: 'apple-touch-icon',
                        sizes: '180x180',
                        href: 'favicons/touch-icon-180x180-iphone-6-plus.png',
                    },
                },
              {tag: 'title', content: json.title},
              {elem: 'ua'},
              json.head,
              json.styles,
            ],
          },
          json,
        ],
      },
    ];
  });
};
