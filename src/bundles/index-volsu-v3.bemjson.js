var nav = ['Абитуриенту', 'Студенту', 'Аспиранту', 'Сотруднику',
    'Выпускнику', 'Партнеру', 'Волонтеру'];

var achieveItems = ['ВолГУ основан в 1980 году', '58 докторов наук',
    '117 кандидатов наук', '3 089 сотрудников', '5 086 научных публикаций',
    '19 700 студентов и аспирантов'];

var filters = [
    {content: 'Конференции', filter: 'conference'},
    {content: 'Международная деятельность', filter: 'international'},
    {content: 'Олимпиады', filter: 'olympiads'},
    {content: 'Волонтерам', filter: 'volunteers'}
];

var topNews = ['olympiads, conference', 'international', 'volunteers',
    'conference'];

var news = [
    {filter: 'olympiads, conference', img: '1', descr: 'Завершился летний оздоровительный отдых студентов ВолГУ на Черноморском побережье'},
    {filter: 'olympiads', img: '2', descr: 'Студенты и преподаватели ВолГУ отправились в экспедицию «Волжского плавучего университета»'},
    {filter: 'olympiads', img: '3', descr: 'В ВолГУ завершены вступительные испытания в магистратуру'},
    {filter: 'olympiads', img: '4', descr: 'Студенты и преподаватели ВолГУ отправились в экспедицию «Волжского плавучего университета»'},
    {filter: 'olympiads', img: '11'},
    {filter: 'conference', img: '2'},
    {filter: 'international', img: '4'},
    {filter: 'volunteers', img: '9'},
    {filter: 'conference', img: '7'},
    {filter: 'olympiads', img: '8'},
    {filter: 'international', img: '4'},
    {filter: 'volunteers', img: '9'},
    {filter: 'conference', img: '13'},
    {filter: 'olympiads', img: '12'},
    {filter: 'international', img: '10'},
    {filter: 'volunteers', img: '5'},
    {filter: 'conference', img: '5'},
    {filter: 'olympiads', img: '10'},
    {filter: 'international', img: '12'},
    {filter: 'volunteers', img: '13'},
    {filter: 'conference', img: '9'},
    {filter: 'olympiads', img: '4'},
    {filter: 'international', img: '8'},
    {filter: 'volunteers', img: '7'},
    {filter: 'conference', img: '9'},
    {filter: 'olympiads', img: '4'},
    {filter: 'international', img: '2'},
    {filter: 'volunteers', img: '11'},
];

var tmpDescr = 'Студенты и преподаватели ВолГУ отправились в экспедицию ' +
    '«Волжского плавучего университета»';

var enrItems = ['Правила приема в 2017 году',
    'Образовательные программы в 2017 году',
    'Количество мест в 2017 году (планы приема)', 'Договорнику (тарифы)',
    'Списки поступающих в 2017 году',
    'Информация о способах подачи документов в 2017 году',
    'Приказы о зачислении в 2017 году',
    'Для победителей (призеров) олимпиад школьников'];

var socials = [
    {src: '1', type: 'inst', time: '17 часов назад', text: 'БОЛЬШОЕ СПАСИБО,' +
    'за Ваш труд,Вашу работу,Ваш вклад и развитие волонтерского центра!'},
    {src: '2', type: 'f', time: '7 дней назад', text: '#общагастайл #8этаж' +
    '#Волгу #ВолГу #волгуобщага #волгуобъединяет'},
    {src: '3', type: 'vk', time: '8 дней назад', text: '#общагастайл #8этаж ' +
    '#Волгу #ВолГу #волгуобщага #волгуобъединяет'},
    {src: '4', type: 'ok', time: '10 дней назад', text: '#общагастайл #8этаж ' +
    '#Волгу #ВолГу #волгуобщага #волгуобъединяет'},
    {src: '5', type: 'g', time: '15 дней назад', text: '#общагастайл #8этаж ' +
    '#Волгу #ВолГу #волгуобщага #волгуобъединяет'}
];

var linkBlocks = [
    {src: 'library', title: 'Библиотека ВолГУ'},
    {src: 'sdo', title: 'Система дистанционного образования «Умка»'},
    {src: 'paper', title: 'Вестник ВолГУ'},
    {src: 'sdo', title: 'Система дистанционного образования «Умка»'},
    {src: 'library', title: 'Библиотека ВолГУ'},
    {src: 'sdo', title: 'Система дистанционного образования «Умка»'}
];

module.exports = {
    block: 'page',
    title: 'Главная',
    content: [
        {
            block: 'header',
            content: [
                require('./common/header.bemjson.js'),
            ],
         },
        {
            block: 'line',
            content: {
                cls: 'container',
                content: {
                    block: 'main-slider',
                    content: [
                        {
                            elem: 'images-wrapper',
                            content: [(function() {
                                    var res = [];

                                    for(var i=0; i<6; i++) {
                                        res.push({
                                            block: 'link',
                                            tag: 'div',
                                            slide: i.toString(),
                                            content: {
                                                block: 'img',
                                                src: 'images/main-slider/1.jpg',
                                            },
                                        });
                                    }

                                    return res;
                                })()
                            ],
                        },
                        {
                            block: 'slider',
                            mods: {main: true},
                            content: [
                                {content: [
                                    {cls: 'swiper-container', content: [
                                        {
                                            elem: 'wrapper',
                                            cls: 'swiper-wrapper',
                                            content: [(function() {
                                                var res = [];

                                                for(var i=0; i<6; i++) {
                                                    res.push({
                                                        elem: 'slide',
                                                        cls: 'swiper-slide',
                                                        content: {
                                                            elem: 'text-wrapper',
                                                            content: [
                                                                {
                                                                    elem: 'header',
                                                                    content: 'ВолГУ: ' +
                                                                    'летопись ' +
                                                                    'достижений ' +
                                                                    '2016/2017',
                                                                },
                                                                {
                                                                    elem: 'text',
                                                                    content: '24 мая ' +
                                                                    '– знаменательная' +
                                                                    ' дата для ' +
                                                                    'студентов и ' +
                                                                    'преподавателей ' +
                                                                    'ВолГУ. Ежегодно ' +
                                                                    'в День славянской' +
                                                                    ' письменности ' +
                                                                    'все ' +
                                                                    'университетское' +
                                                                    ' сообщество' +
                                                                    ' празднует день' +
                                                                    ' родного ВолГУ.',
                                                                },
                                                            ],
                                                        },
                                                    });
                                                }

                                                return res;
                                            })()],
                                        },
                                    ]},
                                ]},
                                {
                                    elem: 'control',
                                    content: [
                                        {
                                            elem: 'arrow',
                                            mix: {block: 'icon'},
                                            cls: 'icon-arr_left',
                                            elemMods: {arrow: 'left'},
                                        },
                                        {
                                            elem: 'pagination',
                                        },
                                        {
                                            elem: 'arrow',
                                            mix: {block: 'icon'},
                                            cls: 'icon-arr_right',
                                            elemMods: {arrow: 'right'},
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            },
        },
        {
            block: 'navigation-wrapper',
            mods: {style: 'content'},
            content: [
                {
                    block: 'navigation',
                    content: {
                        cls: 'container',
                        content: {
                            elem: 'header',
                            content: [
                                {
                                    elem: 'brand',
                                    content: 'Навигация',
                                },
                                {
                                    block: 'btn',
                                    mods: {transparent: true},
                                    content: [
                                        { elem: 'bar' },
                                        { elem: 'bar' },
                                        { elem: 'bar' },
                                    ],
                                },
                            ],
                        },
                    },
                },
                {
                    block: 'navigation',
                    mods: {collapse: true},
                    content: {
                        cls: 'container',
                        content: {
                            elem: 'list',
                            content: [(function() {
                                var res = [];

                                for(var i=0; i<nav.length; i++) {
                                    res.push({
                                        elem: 'item',
                                        content: {
                                            elem: 'link',
                                            content: nav[i],
                                        },
                                    },);
                                }

                                return res;
                            })()],
                        },
                    },
                },
            ],
        },
        {
            content: {
                cls: 'container',
                content: {
                    block: 'achievements',
                    content: [
                        {
                            elem: 'box',
                            elemMods: {show: 'list'},
                            content: [(function() {
                                var res = [];

                                for(var i=0; i<achieveItems.length; i++) {
                                    res.push({
                                        elem: 'item',
                                        content: achieveItems[i],
                                    },);
                                }

                                return res;
                            })()],
                        },
                        {
                            elem: 'box',
                            elemMods: {show: 'banners'},
                            content: [
                                {
                                    elem: 'img',
                                    src: 'images/achievements/QS_BRICS.png',
                                },
                                {
                                    elem: 'img',
                                    src: 'images/achievements/QS_stars.png',
                                },
                                {
                                    elem: 'img',
                                    src: 'images/achievements/Webometrics.png',
                                },
                                {
                                    elem: 'img',
                                    src: 'images/achievements/QS_EECA.png',
                                },
                                {
                                    elem: 'img',
                                    src: 'images/achievements/U_Multirank_' +
                                    'claim_rgb.png',
                                },
                                {
                                    elem: 'img',
                                    src: 'images/achievements/Rank_ProX.png',
                                },
                                {
                                    elem: 'row',
                                    elemMods: {width: true},
                                    content: [
                                        {
                                            elem: 'img',
                                            src: 'images/achievements/' +
                                            'Teaching5.png',
                                        },
                                        {
                                            elem: 'row',
                                            elemMods: {column: true},
                                            content: [
                                                {
                                                    elem: 'img',
                                                    src: 'images/achievements/' +
                                                    'logo-raex.png',
                                                },
                                                {
                                                    elem: 'row',
                                                    content: [
                                                        {
                                                            elem: 'img',
                                                            src: 'images/' +
                                                            'achievements/' +
                                                            'Fond-Potanina.png',
                                                        },
                                                        {
                                                            elem: 'img',
                                                            src: 'images/' +
                                                            'achievements/' +
                                                            'interfax.png',
                                                        },
                                                        {
                                                            elem: 'img',
                                                            src: 'images/' +
                                                            'achievements/' +
                                                            'ares.png',
                                                        },
                                                    ],
                                                },
                                            ],
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            },
        },
        {
            block: 'news',
            content: {
                cls: 'container',
                content: [
                    {
                        elem: 'header',
                        content: [
                            {
                                block: 'block-header',
                                content: [
                                    'Новости',
                                    {
                                        block: 'mark',
                                        type: 'link',
                                        content: 'Все новости',
                                    },
                                ],
                            },
                            {
                                block: 'filter',
                                content: [(function() {
                                    var res = [];

                                    for(var i=0; i<filters.length; i++) {
                                        res.push({
                                            elem: 'item',
                                            content: {
                                                elem: 'control',
                                                content: filters[i].content,
                                                filter: filters[i].filter,
                                            },
                                        });
                                    }

                                    return res;
                                })()],
                            },
                        ],
                    },
                    {
                        elem: 'content',
                        content: [
                            {
                                elem: 'top-news',
                                content: [(function() {
                                    var res = [];

                                    for(var i=0; i<topNews.length; i++) {
                                        res.push({
                                            elem: 'item-box',
                                            content: {
                                                elem: 'item',
                                                filter: topNews[i],
                                                content: [
                                                    {
                                                        block: 'triangle',
                                                        mods: {size: 'big'},
                                                        src: '',
                                                    },
                                                    {
                                                        elem: 'triangle-shadow',
                                                        mix: {block: 'icon'},
                                                        cls: 'icon-big_triangle',
                                                    },
                                                    {
                                                        elem: 'descr',
                                                        content: '',
                                                    },
                                                ],
                                            },
                                        });
                                    }

                                    return res;
                                })()],
                            },

                            (function() {
                                var res = [];

                                for(var i=0; i<news.length; i++) {
                                    res.push({
                                        elem: 'item-box',
                                        content: {
                                            elem: 'item',
                                            elemMods: {little: true},
                                            filter: news[i].filter,
                                            content: [
                                                {
                                                    elem: 'photo-wrapper',
                                                    content: {
                                                        elem: 'photo',
                                                        src: 'http://placehold.it/65x65',
                                                        attrs: {
                                                            'data-large': 'http://placehold.it/260x260',
                                                        }
                                                    },
                                                },
                                                {
                                                    elem: 'descr-wrapper',
                                                    content: [
                                                        {
                                                            block: 'icon',
                                                            cls: 'icon-forma',
                                                        },
                                                        {
                                                            elem: 'descr',
                                                            content: (news[i].descr) ? news[i].descr : tmpDescr,
                                                        },
                                                    ],
                                                },
                                            ],
                                        },
                                    });
                                }

                                return res;
                            })(),
                        ],
                    },
                ],
            },
        },
        {
            block: 'events-wrapper',
            content: {
                cls: 'container',
                content: [
                    {
                        block: 'block-header',
                        content: [
                            'Ближайшие события',
                            {
                                block: 'mark',
                                type: 'link',
                                content: 'Все события',
                            },
                        ],
                    },
                    {
                        block: 'events',
                        content: [
                            {
                                elem: 'box',
                                content: [
                                    {
                                        elem: 'date',
                                        content: '18.08.2017',
                                    },
                                    {
                                        elem: 'text',
                                        mix: {block: 'icon'},
                                        cls: 'icon-triangle',
                                        content: {
                                            elem: 'link',
                                            content: 'В Волгоградском ' +
                                            'государственном университете ' +
                                            '20-21 октября 2017 г. пройдет ' +
                                            'IV Международная научная ' +
                                            'конференция «Военная история ' +
                                            'России: проблемы, поиски, ' +
                                            'решения», посвященная 75-летию' +
                                            ' победы в Сталинградской битве.',
                                        },
                                    },
                                ],
                            },
                            {
                                elem: 'box',
                                content: [
                                    {
                                        elem: 'date',
                                        content: '15.08.2017',
                                    },
                                    {
                                        elem: 'text',
                                        mix: {block: 'icon'},
                                        cls: 'icon-triangle',
                                        content: {
                                            elem: 'link',
                                            content: 'С 3 по 10 апреля ' +
                                            'экспертными комиссиями, ' +
                                            'сформированными из авторитетных ' +
                                            'учёных и специалистов в ' +
                                            'соответствующей области, ' +
                                            'проведено заочное рассмотрение ' +
                                            'зарегистрированных работ для ' +
                                            'участия в Научной сессии ВолГУ ' +
                                            '2017 г.',
                                        },
                                    },
                                ],
                            },
                            {
                                elem: 'box',
                                content: [
                                    {
                                        elem: 'date',
                                        content: '10.08.2017',
                                    },
                                    {
                                        elem: 'text',
                                        mix: {block: 'icon'},
                                        cls: 'icon-triangle',
                                        content: {
                                            elem: 'link',
                                            content: 'В Волгоградском ' +
                                            'государственном университете ' +
                                            '20-21 октября 2017г. пройдет ' +
                                            'IV Международная научная' +
                                            ' конференция «Военная история ' +
                                            'России: проблемы, поиски, ' +
                                            'решения», посвященная 75- летию' +
                                            ' победы в Сталинградской битве.',
                                        },
                                    },
                                ],
                            },
                            {
                                elem: 'box',
                                content: [
                                    {
                                        elem: 'date',
                                        content: '5.08.2017',
                                    },
                                    {
                                        elem: 'text',
                                        mix: {block: 'icon'},
                                        cls: 'icon-triangle',
                                        content: {
                                            elem: 'link',
                                            content: 'С 3 по 10 апреля ' +
                                            'экспертными комиссиями, ' +
                                            'сформированными из авторитетных ' +
                                            'учёных и специалистов в ' +
                                            'соответствующей области, ' +
                                            'проведено заочное рассмотрение ' +
                                            'зарегистрированных работ для ' +
                                            'участия в Научной сессии ' +
                                            'ВолГУ 2017г.',
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        },
        {
            block: 'enrollees',
            mix: {block: 'contrast'},
            content: {
                cls: 'container',
                content: [
                    {
                        block: 'block-header',
                        content: 'Абитуриентам и магистрантам',
                    },
                    {
                        elem: 'content',
                        content: [
                            {
                                elem: 'box',
                                content: [(function() {
                                    var res = [];

                                    for(var i=0; i<enrItems.length; i++) {
                                        res.push({
                                            elem: 'item',
                                            content: {
                                                elem: 'link',
                                                content: enrItems[i],
                                            },
                                        },);
                                    }

                                    return res;
                                })()],
                            },
                            {
                                elem: 'box',
                                content: [
                                    {
                                        block: 'important-day',
                                        content: [
                                            {
                                                elem: 'header',
                                                content: 'Ближайшие важные даты',
                                            },
                                            {
                                                elem: 'date',
                                                content: '8 августа 2017 года',
                                            },
                                            {
                                                elem: 'event',
                                                content: 'приказ о ' +
                                                'зачислении на втором этапе ' +
                                                '(бакалавриат, ' +
                                                'очная/очно-заочная форма ' +
                                                'обучения)',
                                            },
                                            {
                                                elem: 'event',
                                                content: 'окончание приема ' +
                                                'документов у лиц, ' +
                                                'поступающих по ЕГЭ (заочная' +
                                                ' форма, бюджет)',
                                            },
                                        ],
                                    },
                                    {
                                        block: 'hotline',
                                        content: [
                                            {
                                                elem: 'header',
                                                content: 'Горячая линия по' +
                                                ' вопросам:',
                                            },
                                            {
                                                elem: 'phone-box',
                                                content: [
                                                    {
                                                        elem: 'phone',
                                                        content: '8 (8442) ' +
                                                        '460-279',
                                                        tel: '+78442460279',
                                                    },
                                                    {
                                                        elem: 'who',
                                                        content: 'приемная ' +
                                                        'ректора',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'phone-box',
                                                content: [
                                                    {
                                                        elem: 'phone',
                                                        content: '8 (8442)' +
                                                        ' 460-279',
                                                        tel: '+78442460279',
                                                    },
                                                    {
                                                        elem: 'who',
                                                        content: 'приемная' +
                                                        ' комиссия',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    }
                ],
            },
        },
        {
            content: {
                cls: 'container',
                content: {
                    block: 'around',
                    content: [
                        {
                            block: 'block-header',
                            content: 'Вокруг ВолГУ',
                        },
                        {
                            block: 'slider-socials',
                            mix: {block: 'slider'},
                            content: [
                                {
                                    elem: 'arrow-box',
                                    mix: {block: 'slider', elem: 'arrow-box'},
                                    elemMods: {arr: 'left'},
                                    content: {
                                        elem: 'arrow',
                                        mix: [
                                            {block: 'icon'},
                                            {block: 'slider', elem: 'arrow'}],
                                        cls: 'icon-arr_left',
                                    },
                                },
                                {
                                    elem: 'wrapper',
                                    content: [(function() {
                                        var res = [];

                                        for(var i=0; i<socials.length; i++) {
                                            res.push({
                                                elem: 'slide',
                                                content: {
                                                    block: 'social-talk',
                                                    content: [
                                                        {
                                                            elem: 'photo',
                                                            src: 'images/' +
                                                            'social-photo/' +
                                                            socials[i].src +
                                                            '.jpg',
                                                        },
                                                        {
                                                            elem: 'net',
                                                            elemMods: {
                                                                type: socials[i].type
                                                            },
                                                        },
                                                        {
                                                            elem: 'name',
                                                            content: '@alesastro',
                                                        },
                                                        {
                                                            elem: 'text',
                                                            content: socials[i].text,
                                                        },
                                                        {
                                                            elem: 'time',
                                                            content: socials[i].time,
                                                        },
                                                    ],
                                                },
                                            },);
                                        }

                                        return res;
                                    })()],
                                },
                                {
                                    elem: 'arrow-box',
                                    mix: {block: 'slider', elem: 'arrow-box'},
                                    mods: {arr: 'right'},
                                    content: {
                                        elem: 'arrow',
                                        mix: [
                                            {block: 'icon'},
                                            {block: 'slider', elem: 'arrow'}
                                        ],
                                        cls: 'icon-arr_right',
                                    },
                                },
                            ],
                        },
                    ],
                },
            },
        },
        {
            block: 'contrast',
            content: {
                cls: 'container',
                content: {
                    block: 'slider-blocks',
                    mix: {block: 'slider'},
                    content: [
                        {
                            elem: 'wrapper',
                            content: [(function() {
                                var res = [];

                                for(var i=0; i<linkBlocks.length; i++) {
                                    res.push({
                                        elem: 'slide',
                                        content: {
                                            block: 'link-block',
                                            content: [
                                                {
                                                    block: 'img',
                                                    src: 'images/link-img/' +
                                                    linkBlocks[i].src + '.jpg',
                                                },
                                                {
                                                    elem: 'link',
                                                    content: linkBlocks[i].title,
                                                },
                                            ],
                                        },
                                    });
                                }

                                return res;
                            })()],
                        },
                        {
                            elem: 'control',
                            mix: {block: 'slider', elem: 'control'},
                            content: {
                                elem: 'pagination',
                                mix: {block: 'slider', elem: 'pagination'},
                            },
                        },
                    ],
                },
            },
        },
        require('./common/slider_banners.bemjson.js'),
        require('./common/links.bemjson.js'),
        require('./common/footer.bemjson.js'),
        require('./common/modal_feedback.bemjson.js'),
    ],
};
