var tags = [
    {title: 'День рождения', tag: 'bd1'},
    {title: 'ВолГУ', tag: 'volsu1'},
    {title: 'Кружки', tag: 'circ1'},
    {title: 'Кластеры', tag: 'clust1'},
    {title: 'Молодежь', tag: 'youth1'},
    {title: 'Образование', tag: 'edu1'},
    {title: 'Университетский округ', tag: 'distr1'},
    {title: 'Абитуриенты', tag: 'enr1'},
    {title: 'Семинар', tag: 'sem1'},
    {title: 'Молодежь', tag: 'youth2'},
    {title: 'ВолГУ', tag: 'volsu2'},
    {title: 'Кружки', tag: 'circ2'},
    {title: 'Кластеры', tag: 'clust2'},
    {title: 'Молодежь', tag: 'youth3'},
    {title: 'День рождения', tag: 'bd2'},
    {title: 'ВолГУ', tag: 'volsu3'},
    {title: 'Кружки', tag: 'circ3'},
    {title: 'Кластеры', tag: 'clust3'},
    {title: 'Молодежь', tag: 'youth4'},
    {title: 'Образование', tag: 'edu2'},
    {title: 'Университетский округ', tag: 'distr2'},
    {title: 'Абитуриенты', tag: 'enr2'},
    {title: 'Семинар', tag: 'sem2'},
];

module.exports = {
    block: 'page',
    title: 'События ВолГУ',
    content: [
        require('./common/navigation.bemjson.js'),
        {
            block: 'header',
            tag: 'header',
            mods: {border: true},
            content: [
                require('./common/header.bemjson.js'),
            ],
        },
        require('./common/breadcrumbs.bemjson.js'),
        {
            block: 'main',
            mods: {border: true},
            content: {
                cls: 'container',
                content: [
                    require('./common/sidebar.bemjson.js'),
                    {
                        elem: 'content',
                        content: [
                            {
                                elem: 'header-wrapper',
                                content: [
                                    {
                                        elem: 'header',
                                        content: 'События',
                                    },
                                    {
                                        elem: 'sort',
                                        elemMods: {events: true},
                                        content: [
                                            {
                                                block: 'btn',
                                                mods: {sort: true},
                                                attrs: {'data-sort': 'date'},
                                                content: [
                                                    'По дате',
                                                    {
                                                        block: 'icon',
                                                        tag: 'i',
                                                        cls: 'icon-arr',
                                                    },
                                                ],
                                            },
                                            {
                                                block: 'btn',
                                                mods: {sort: true},
                                                attrs: {'data-sort': 'popular'},
                                                content: [
                                                    'По популярности',
                                                    {
                                                        block: 'icon',
                                                        tag: 'i',
                                                        cls: 'icon-arr_top',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                elem: 'header-wrapper',
                                content: [
                                    {
                                        block: 'btn-group',
                                        content: [(function() {
                                            var res = [];
                                            var btns = ['Сегодня',
                                                        'Вчера',
                                                        'Неделя',
                                                        'Месяц',
                                                        'Квартал',
                                                        'Год'];

                                            for(var i=0; i<btns.length; i++) {
                                                res.push({
                                                    block: 'btn',
                                                    content: btns[i],
                                                });
                                            }

                                            return res;
                                        })()],
                                    },
                                    {
                                        elem: 'calendar-wrapper',
                                        content: [
                                            {
                                                block: 'btn',
                                                mods: {calendar: true},
                                                cls: 'calendar-from',
                                                content: [
                                                    {
                                                        elem: 'text',
                                                        content: 'С даты',
                                                    },
                                                    {
                                                        block: 'icon',
                                                        tag: 'i',
                                                        cls: 'icon-arr',
                                                    },
                                                    require('./common/calenda' +
                                                        'r.bemjson.js'),
                                                ],
                                            },
                                            {
                                                block: 'btn',
                                                mods: {calendar: true},
                                                cls: 'calendar-to',
                                                content: [
                                                    {
                                                        elem: 'text',
                                                        content: 'По дату',
                                                    },
                                                    {
                                                        block: 'icon',
                                                        tag: 'i',
                                                        cls: 'icon-arr',
                                                    },
                                                    require('./common/calenda' +
                                                        'r.bemjson.js'),
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'link',
                                        mods: {'toggle-tags': true},
                                        content: 'Показать теги',
                                        attrs: {
                                            'data-show': 'Показать теги',
                                            'data-hide': 'Скрыть теги',
                                        },
                                    },
                                ],
                            },
                            {
                                block: 'tags',
                                content: [
                                    {
                                        elem: 'list',
                                        content: [(function() {
                                            var res = [];

                                            for(var i=0; i<tags.length; i++) {
                                                res.push({
                                                    elem: 'item',
                                                    attrs: {
                                                        'data-tag': tags[i].tag,
                                                    },
                                                    content: [
                                                        {
                                                            block: 'link',
                                                            content:
                                                            tags[i].title,
                                                        },
                                                        {
                                                            block: 'icon',
                                                            tag: 'i',
                                                            cls: 'icon-close',
                                                        },
                                                    ],
                                                });
                                            }

                                            return res;
                                        })()],
                                    },
                                    {
                                        block: 'form',
                                        mix: {
                                            block: 'tags',
                                            elem: 'subscribe',
                                        },
                                        content: [
                                            {
                                                block: 'icon',
                                                cls: 'icon-mail half-primary',
                                            },
                                            {
                                                block: 'icon',
                                                cls: 'icon-mail half-primary',
                                            },
                                            {
                                                block: 'icon',
                                                cls: 'icon-mail secondary',
                                            },
                                            {
                                                block: 'input',
                                                mods: {
                                                    'mail-subscribe': true,
                                                },
                                                attrs: {
                                                    placeholder: 'Следите за ' +
                                                    'событиями по выбранным ' +
                                                    'тегам',
                                                    type: 'email',
                                                },
                                            },
                                            {
                                                block: 'btn',
                                                mods: {
                                                    secondary: true,
                                                    subscribe: true,
                                                },
                                                content: 'Подписаться',
                                            },
                                            {
                                                block: 'icon',
                                                cls: 'icon-mail secondary',
                                            },
                                            {
                                                block: 'icon',
                                                cls: 'icon-mail half-primary',
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                elem: 'grid',
                                content: [
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '1.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'День рожден' +
                                                        'ия ВолГУ',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'В рамках ре' +
                                                        'ализации Концепции о' +
                                                        'бщенациональной сист' +
                                                        'емы выявления и разв' +
                                                        'ития молодых таланто' +
                                                        'в Волгоградский госу' +
                                                        'дарственный универси' +
                                                        'тет выступил с иници' +
                                                        'ативой создания в Во' +
                                                        'лгоградской области ' +
                                                        'комплексного проекта',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'bd1',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'volsu1',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'circ1',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '2.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Предметные ' +
                                                        'кружки для школьников',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Целью проек' +
                                                        'та является объедине' +
                                                        'ние усилий образоват' +
                                                        'ельных и общественны' +
                                                        'х организаций, орган' +
                                                        'ов исполнительной вл' +
                                                        'асти, заинтересованн' +
                                                        'ых в развитии одарён' +
                                                        'ных детей и талантли' +
                                                        'вой молодёжи, в подг' +
                                                        'отовке ее к созидате' +
                                                        'льной деятельности н' +
                                                        'а благо',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'bd2',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'volsu2',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'distr1',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '3.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Олимпийская' +
                                                        ' смена',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Организацио' +
                                                        'нно университетский ' +
                                                        'округ состоит из кла' +
                                                        'стеров (группы образ' +
                                                        'овательных организац' +
                                                        'ий общего, среднего ' +
                                                        'профессионального и ' +
                                                        'дополнительного обра' +
                                                        'зования), объединенн' +
                                                        'ых вокруг опорной ор' +
                                                        'ганизации, в роли ко' +
                                                        'торой может выступат' +
                                                        'ь школа, гимназия, л' +
                                                        'ицей',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'bd1',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'volsu3',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'clust1',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '1.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'День ' +
                                                        'рождения ВолГУ',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'В рамках ре' +
                                                        'ализации Концепции о' +
                                                        'бщенациональной сист' +
                                                        'емы выявления и разв' +
                                                        'ития молодых таланто' +
                                                        'в Волгоградский госу' +
                                                        'дарственный универси' +
                                                        'тет выступил с иници' +
                                                        'ативой создания в Во' +
                                                        'лгоградской области ' +
                                                        'комплексного проекта',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'bd2',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'volsu1',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'edu1',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '2.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Предметные ' +
                                                        'кружки для школьников',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Целью проек' +
                                                        'та является объедине' +
                                                        'ние усилий образоват' +
                                                        'ельных и общественны' +
                                                        'х организаций, орган' +
                                                        'ов исполнительной вл' +
                                                        'асти, заинтересованн' +
                                                        'ых в развитии одарён' +
                                                        'ных детей и талантли' +
                                                        'вой молодёжи, в подг' +
                                                        'отовке ее к созидате' +
                                                        'льной деятельности н' +
                                                        'а благо',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'bd1',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'volsu1',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'edu2',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '3.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Олимпийская' +
                                                        ' смена',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Организацио' +
                                                        'нно университетский ' +
                                                        'округ состоит из кла' +
                                                        'стеров (группы образ' +
                                                        'овательных организац' +
                                                        'ий общего, среднего ' +
                                                        'профессионального и ' +
                                                        'дополнительного обра' +
                                                        'зования), объединенн' +
                                                        'ых вокруг опорной ор' +
                                                        'ганизации, в роли ко' +
                                                        'торой может выступат' +
                                                        'ь школа, гимназия, л' +
                                                        'ицей',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'bd2',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'volsu2',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'youth3',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '1.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'День ' +
                                                        'рождения ВолГУ',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'В рамках ре' +
                                                        'ализации Концепции о' +
                                                        'бщенациональной сист' +
                                                        'емы выявления и разв' +
                                                        'ития молодых таланто' +
                                                        'в Волгоградский госу' +
                                                        'дарственный универси' +
                                                        'тет выступил с иници' +
                                                        'ативой создания в Во' +
                                                        'лгоградской области ' +
                                                        'комплексного проекта',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'circ3',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'clust3',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'youth4',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '2.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Предметные ' +
                                                        'кружки для школьников',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Целью проек' +
                                                        'та является объедине' +
                                                        'ние усилий образоват' +
                                                        'ельных и общественны' +
                                                        'х организаций, орган' +
                                                        'ов исполнительной вл' +
                                                        'асти, заинтересованн' +
                                                        'ых в развитии одарён' +
                                                        'ных детей и талантли' +
                                                        'вой молодёжи, в подг' +
                                                        'отовке ее к созидате' +
                                                        'льной деятельности н' +
                                                        'а благо',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'sem1',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'enr1',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'youth2',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        block: 'card',
                                        content: [
                                            {
                                                elem: 'img',
                                                content: [
                                                    {
                                                        block: 'img',
                                                        src: './images/media/' +
                                                        '3.jpg',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'body',
                                                content: [
                                                    {
                                                        elem: 'date',
                                                        content: '24.05.2012',
                                                    },
                                                    {
                                                        elem: 'title',
                                                        content: 'Олимпийская' +
                                                        ' смена',
                                                    },
                                                    {
                                                        elem: 'text',
                                                        content: 'Организацио' +
                                                        'нно университетский ' +
                                                        'округ состоит из кла' +
                                                        'стеров (группы образ' +
                                                        'овательных организац' +
                                                        'ий общего, среднего ' +
                                                        'профессионального и ' +
                                                        'дополнительного обра' +
                                                        'зования), объединенн' +
                                                        'ых вокруг опорной ор' +
                                                        'ганизации, в роли ко' +
                                                        'торой может выступат' +
                                                        'ь школа, гимназия, л' +
                                                        'ицей',
                                                    },
                                                ],
                                            },
                                            {
                                                elem: 'footer',
                                                content: [
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'bd2',
                                                        },
                                                        content: 'День ' +
                                                        'рождения',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag':
                                                                'distr2',
                                                        },
                                                        content: 'ВолГУ',
                                                    },
                                                    {
                                                        block: 'mark',
                                                        attrs: {
                                                            'data-tag': 'circ3',
                                                        },
                                                        content: '2017',
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                block: 'btn',
                                mods: {secondary: true, more: true},
                                content: [
                                    'Еще материалы',
                                    {
                                        block: 'icon',
                                        tag: 'i',
                                        cls: 'icon-arr',
                                    },
                                ],
                            },
                            require('./common/pagination.bemjson.js'),
                        ],
                    },
                ],
            },
        },
        require('./common/slider_banners.bemjson.js'),
        require('./common/links.bemjson.js'),
        require('./common/footer.bemjson.js'),
        require('./common/modal_feedback.bemjson.js'),
    ],
};
