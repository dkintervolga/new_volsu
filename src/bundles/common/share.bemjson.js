var socials = [
    {icon: 'vk', title: 'во ВКонтакте', link: 'https://vk.com/share.php?url='},
    {icon: 'f', title: 'в Facebook', link: 'https://www.facebook.com/sharer/sharer.php?u='},
    {icon: 'tw', title: 'в Twitter', link: 'https://twitter.com/intent/tweet?text='},
];

module.exports = {
    block: 'share',
    mods: {hide: true},
    content: {
        elem: 'wrapper',
        content: [(function() {
            var res = [];

            for(var i=0; i<socials.length; i++) {
                res.push({
                    block: 'link',
                    attrs: {
                        href: socials[i].link,
                        title: 'Опубликовать ссылку ' + socials[i].title,
                    },
                    mix: {
                        block: 'icon',
                    },
                    cls: 'icon-' + socials[i].icon,
                });
            }

            return res;
        })()],
    },
};
