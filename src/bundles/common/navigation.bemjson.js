var items = ['Абитуриенту', 'Студенту', 'Аспиранту', 'Сотруднику',
    'Выпускнику', 'Партнеру', 'Волонтеру'];

module.exports = {
    block: 'navigation-wrapper',
    mods: {style: 'full-length'},
    content: [
        {
            block: 'navigation',
            content: {
                cls: 'container',
                content: {
                    elem: 'header',
                    content: [
                        {
                            elem: 'brand',
                            content: 'Навигация',
                        },
                        {
                            block: 'btn',
                            mods: {transparent: true},
                            content: [
                                {elem: 'bar'},
                                {elem: 'bar'},
                                {elem: 'bar'},
                            ],
                        },
                    ],
                },
            },
        },
        {
            block: 'navigation',
            mods: {collapse: true},
            content: {
                cls: 'container',
                content: {
                    elem: 'list',
                    content: [(function() {
                        var res = [];

                        for(var i=0; i<items.length; i++) {
                            res.push({
                                elem: 'item',
                                content: {
                                    elem: 'link',
                                    content: items[i],
                                },
                            });
                        }

                        return res;
                    })()],
                },
            },
        },
    ],
};
