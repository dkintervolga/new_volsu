module.exports = {
  cls: 'container',
  content: [
    {
      elem: 'controls',
      content: [
        {
          elem: 'menu',
          content: [
            {
              block: 'btn',
              mods: {transparent: true},
              content: [
                {elem: 'bar'},
                {elem: 'bar'},
                {elem: 'bar'},
              ],
            },
            require('./logo.bemjson'),
          ],
        },
        {
          elem: 'control-box',
          content: {
            elem: 'org-info',
            content: 'Сведения об образовательной' +
            ' организации',
          },
        },
        {
          block: 'settings',
          content: [
            {
              block: 'icon',
              mods: {href: true},
              cls: 'icon-eye',
            },
            {
              elem: 'search',
              content: [
                {
                  block: 'icon',
                  cls: 'icon-search',
                },
                {
                  block: 'input',
                  mods: {search: true, hidden: true},
                },
              ],
            },
            {
              block: 'triangle',
              mods: {size: 'sm', href: true},
              src: 'images/lang/china.png',
              id: '2',
            },
            {
              block: 'triangle',
              mods: {size: 'sm', href: true},
              src: 'images/lang/gb.png',
              id: '1',
            },
          ],
        },
      ],
    },
    {
      block: 'sub-navigation',
      content: [
        {
          elem: 'item',
          content: {
            elem: 'link',
            content: 'Образование',
          },
        },
        {
          elem: 'item',
          content: {
            elem: 'link',
            content: 'Наука',
          },
        },
        {
          elem: 'item',
          elemMods: {selected: true},
          content: {
            elem: 'link',
            content: 'Университет и общество',
          },
        },
        {
          elem: 'item',
          content: {
            elem: 'link',
            content: 'Международная деятельность',
          },
        },
      ],
    },
  ],
};
