var slides = [1, 2, 3, 4, 1, 2];

module.exports = {
    content: {
        cls: 'container',
        content: {
            block: 'slider-banners',
            mix: {block: 'slider'},
            content: [
                {
                    elem: 'arrow-box',
                    mix: {block: 'slider', elem: 'arrow-box'},
                    content: {
                        elem: 'arrow',
                        mix: [{block: 'icon'}, {block: 'slider', elem: 'arrow'}],
                        cls: 'icon-arr_left',
                    },
                },
                {
                    elem: 'wrapper',
                    content: [(function() {
                        var res = [];

                        for(var i=0; i<slides.length; i++) {
                            res.push({
                                elem: 'slide',
                                content: {
                                    elem: 'link',
                                    content: {
                                        elem: 'img',
                                        src: 'images/slider/sl' + slides[i] + '.jpg',
                                    },
                                },
                            });
                        }

                        return res;
                    })()],
                },
                {
                    elem: 'arrow-box',
                    mix: {block: 'slider', elem: 'arrow-box'},
                    content: {
                        elem: 'arrow',
                        mix: [{block: 'icon'}, {block: 'slider', elem: 'arrow'}],
                        cls: 'icon-arr_right',
                    },
                },
            ],
        },
    },
};
