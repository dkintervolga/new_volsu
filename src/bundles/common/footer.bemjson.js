var icons = ['vk', 'f', 'tw', 'inst', 'youtube'];

var dropdown = ['Библиотека ВолГУ', 'Вестник ВолГУ', 'СДО «Умка»'];

module.exports = {
    block: 'footer',
    content: {
        cls: 'container',
        content: {
            elem: 'content',
            content: [
                {
                    elem: 'box',
                    elemMods: {half: true},
                    content: {
                        elem: 'box-address',
                        content: [
                            require('./logo.bemjson'),
                            {
                                elem: 'text-address',
                                content: '© 2000-2017 Волгоградский ' +
                                'государственный университет',
                            },
                            {
                                elem: 'text-address',
                                content: 'Адрес: 400062, г. Волгоград, пр-т' +
                                ' Университетский, 100,',
                            },
                        ],
                    },
                },
                {
                    elem: 'box-socials',
                    content: [
                        {
                            elem: 'header',
                            content: 'Мы в соц. сетях',
                        },
                        {
                            block: 'socials',
                            content: [(function() {
                                var res = [];

                                for(var i=0; i<icons.length; i++) {
                                    res.push({
                                        elem: 'item',
                                        content: {
                                            elem: 'link',
                                            mix: {block: 'icon'},
                                            cls: 'icon-' + icons[i],
                                        },
                                    });
                                }

                                return res;
                            })()],
                        },
                        {
                            block: 'btn',
                            mods: {secondary: true, feedback: true},
                            content: 'Обратная связь',
                        },
                        {
                            block: 'dropdown',
                            mods: {feedback: true},
                            content: [
                                'Все сайты ВолГУ',
                                {
                                    elem: 'list',
                                    content: [(function() {
                                        var res = [];

                                        for(var i=0; i<dropdown.length; i++) {
                                            res.push({
                                                elem: 'item',
                                                content: {
                                                    elem: 'link',
                                                    content: dropdown[i],
                                                },
                                            });
                                        }

                                        return res;
                                    })()],
                                },
                            ],
                        },
                    ],
                },
                {
                    elem: 'box',
                    content: [
                        {
                            elem: 'header',
                            content: 'Телефоны для справок:',
                        },
                        {
                            elem: 'phone-box',
                            content: [
                                {
                                    elem: 'phone',
                                    content: '8 (8442) 46-02-79',
                                    tel: '+78442460279',
                                },
                                ' &#8212; приемная ректора,',
                            ],
                        },
                        {
                            elem: 'phone-box',
                            content: [
                                {
                                    elem: 'phone',
                                    content: '8 (8442) 46-02-63',
                                    tel: '+78442460263',
                                },
                                ' &#8212; общий отдел,',
                            ],
                        },
                        {
                            elem: 'phone-box',
                            content: [
                                {
                                    elem: 'phone',
                                    content: '8 (8442) 40-55-47',
                                    tel: '+78442405547',
                                },
                                ' &#8212; приемная комиссия,',
                            ],
                        },
                        {
                            elem: 'phone-box',
                            content: [
                                {
                                    elem: 'phone',
                                    content: '8 (8442) 40-55-12',
                                    tel: '+78442405512',
                                },
                                ' &#8212; пресс-служба',
                            ],
                        },
                        {
                            block: 'intervolga',
                        },
                    ],
                },
            ],
        },
    },
};
