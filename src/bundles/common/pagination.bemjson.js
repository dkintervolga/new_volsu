module.exports = {
    block: 'pagination-wrapper',
    content: {
        block: 'pagination',
        mods: {align: 'center'},
        content: [
            {
                elem: 'item',
                elemMods: {disabled: true, end: true},
                cls: 'hidden-xs',
                content: {
                    block: 'link',
                    content: 'Предыдущая',
                },
            },
            {
                elem: 'item',
                elemMods: {disabled: true, end: true},
                cls: 'visible-xs',
                content: {
                    block: 'link',
                    content: '&laquo;',
                },
            },
            {
                elem: 'item',
                elemMods: {active: true},
                content: {
                    block: 'link',
                    content: '1',
                },
            },
            {
                elem: 'item',
                content: {
                    block: 'link',
                    content: '2',
                },
            },
            {
                elem: 'item',
                content: {
                    block: 'link',
                    content: '3',
                },
            },
            {
                elem: 'item',
                content: {
                    block: 'link',
                    content: '4',
                },
            },
            {
                elem: 'item',
                content: {
                    block: 'link',
                    content: '5',
                },
            },
            {
                elem: 'item',
                elemMods: {end: true},
                cls: 'visible-xs',
                content: {
                    block: 'link',
                    content: '&raquo;',
                },
            },
            {
                elem: 'item',
                elemMods: {end: true},
                cls: 'hidden-xs',
                content: {
                    block: 'link',
                    content: 'Следующая',
                },
            },
        ],
    },
};
